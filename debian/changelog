libisfreetype-java (5.6.20131203+dfsg-1) UNRELEASED; urgency=medium

  * Team upload.
  * New upstream version
  TODO: Needs libisrt-java (>= 4.11.20131203)

 -- Andreas Tille <tille@debian.org>  Wed, 05 Feb 2025 18:56:59 +0100

libisfreetype-java (5.3.20100629-4) unstable; urgency=medium

  * Team upload.
  * Point Homepage to webarchive
  * Fix Vcs fields
  * d/copyright: DEP5
  * Obtain upstream source by Files-Excluded
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Add missing build dependency on javahelper for addon javahelper.

 -- Andreas Tille <tille@debian.org>  Wed, 05 Feb 2025 18:53:49 +0100

libisfreetype-java (5.3.20100629-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 12:36:49 +0100

libisfreetype-java (5.3.20100629-3) unstable; urgency=low

  * Team upload.
  * Rebuild with newer Build-Depends. (Closes: #643279)

 -- Torsten Werner <twerner@debian.org>  Tue, 27 Sep 2011 18:10:14 +0200

libisfreetype-java (5.3.20100629-2) unstable; urgency=low

  * Team upload.
  * Rename jar back to isfreetype.jar.

 -- Torsten Werner <twerner@debian.org>  Sun, 04 Sep 2011 01:39:22 +0200

libisfreetype-java (5.3.20100629-1) unstable; urgency=low

  * Team upload
  * New upstream version.
  * Improve the download and cleanup of the orig tarball. Remove javadoc files.
  * Switch to javahelper.
  * Drop versioned package.
  * Update Standards-Version: 3.9.2.
  * Switch to source format 3.0.
  * Downgrade Build-Depends: default-jdk-builddep to default-jdk.

 -- Torsten Werner <twerner@debian.org>  Sat, 03 Sep 2011 18:58:52 +0200

libisfreetype-java (5.2.20091102-2) unstable; urgency=low

  * Jar files should all be lowercase, since so are all references to
    it in web search engines.
  * Added runtime dependency to libfreetype6.
  * Rerelease in response to ftpadmins with hidden x86 binaries in source
    packages removed - thanks to Torsten for spotting that (Closes: #568701).

 -- Steffen Moeller <moeller@debian.org>  Mon, 08 Feb 2010 16:00:12 +0100

libisfreetype-java (5.2.20091102-1) unstable; urgency=low

  * Initial release.

 -- Steffen Moeller <moeller@debian.org>  Sun, 07 Feb 2010 01:07:29 +0100
